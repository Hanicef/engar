package main

import (
	"strings"
	"testing"
)

func TestHandler_Match(t *testing.T) {
	var testMatch = func (h *Handler, s string, ok bool) {
		if h.Match(s) != ok {
			if ok {
				t.Errorf("FAIL: h.Match(\"%s\") != true", s)
			} else {
				t.Errorf("FAIL: h.Match(\"%s\") != false", s)
			}
		}
	}
	h := Handler{
		Wildcards: []string{"*.ft"},
	}

	testMatch(&h, "test.ft", true)
	testMatch(&h, "foo.ft", true)
	testMatch(&h, "foo.txt", false)
	testMatch(&h, "INDEX", false)
	testMatch(&h, "a", false)

	h = Handler{
		Wildcards: []string{"*.png", "*.jpeg", "*.bmp", "*.tiff", "*.gif"},
	}
	testMatch(&h, "test.png", true)
	testMatch(&h, "test.jpeg", true)
	testMatch(&h, "test.bmp", true)
	testMatch(&h, "test.tiff", true)
	testMatch(&h, "test.gif", true)
	testMatch(&h, "test.pdf", false)

	h = Handler{
		Wildcards: []string{"foo*"},
	}
	testMatch(&h, "foobar", true)
	testMatch(&h, "foobaz", true)
	testMatch(&h, "bar", false)

	h = Handler{
		Wildcards: []string{"foo*bar"},
	}
	testMatch(&h, "foobar", true)
	testMatch(&h, "/test/foobar", true)
	testMatch(&h, "fooibar", true)
	testMatch(&h, "/test/fooibar", true)
	testMatch(&h, "fooibaz", false)

	h = Handler{
		Wildcards: []string{"/foo*bar"},
	}
	testMatch(&h, "/foobar", true)
	testMatch(&h, "/test/foobar", false)
	testMatch(&h, "/fooibar", true)
	testMatch(&h, "/test/fooibar", false)
	testMatch(&h, "/fooibaz", false)

	h = Handler{
		Wildcards: []string{"/path/*"},
	}
	testMatch(&h, "/path/hello", true)
	testMatch(&h, "/path/foo", true)
	testMatch(&h, "/foo", false)
	testMatch(&h, "/", false)
	testMatch(&h, "foo", false)
	testMatch(&h, "path/foo", true)
}

func TestSubdivide(t *testing.T) {
	var testSubdivide = func (sd []string, sn ...string) {
		if len(sd) != len(sn) {
			t.Errorf("FAIL: len(sd) != %d", len(sn))
		} else {
			for i, s := range sn {
				if sd[i] != s {
					t.Errorf("sd[%d] != \"%s\" (%s)", i, s, sd[i])
				}
			}
		}
	}
	testSubdivide(Subdivide(0, "foo bar baz"), "foo", "bar", "baz")
	testSubdivide(Subdivide(1, "'lorem ipsum'"), "lorem ipsum")
	testSubdivide(Subdivide(2, "'lorem ipsum' dolor \"sit amet\""), "lorem ipsum", "dolor", "sit amet")
	testSubdivide(Subdivide(3, "'foo'\"bar\"'baz'"), "foobarbaz")
	testSubdivide(Subdivide(4, "'foo\"bar\"baz'"), "foo\"bar\"baz")
}

func TestParseHandlers(t *testing.T) {
	var testHandler = func (h *Handler, w []string, p int, c []string) {
		if len(h.Wildcards) != len(w) {
			t.Errorf("FAIL: len(h.Wildcards) != %d", len(w))
		} else {
			for i := range w {
				if h.Wildcards[i] != w[i] {
					t.Errorf("FAIL: h.Wildcards[i] != %s", w[i])
				}
			}
		}

		if h.Parameter != p {
			t.Errorf("FAIL: h.Parameter != %d", p)
		}

		if len(h.Command) != len(c) {
			t.Errorf("FAIL: len(h.Command) != %d", len(c))
		} else {
			for i := range c {
				if h.Command[i] != c[i] {
					t.Errorf("FAIL: h.Command[i] != %s", c[i])
				}
			}
		}
	}
	file := strings.NewReader("*.ft /bin/forth $\n")
	handlers := ParseHandlers(file)
	if len(handlers) != 1 {
		t.Error("FAIL: len(handlers) != 1")
	} else {
		testHandler(&handlers[0], []string{"*.ft"}, 1, []string{"/bin/forth", "$"})
	}

	file = strings.NewReader("*.py /usr/bin/python3 -E -u $\n*.ft:*.fs:*.fb /bin/forth $ -e bye\n")
	handlers = ParseHandlers(file)
	if len(handlers) != 2 {
		t.Error("FAIL: len(handlers) != 2")
	} else {
		testHandler(&handlers[0], []string{"*.py"}, 3, []string{"/usr/bin/python3", "-E", "-u", "$"})
		testHandler(&handlers[1], []string{"*.ft", "*.fs", "*.fb"}, 1, []string{"/bin/forth", "$", "-e", "bye"})
	}

	file = strings.NewReader("/foo/* /usr/bin/python3 -E -u /resp.py\n")
	handlers = ParseHandlers(file)
	if len(handlers) != 1 {
		t.Error("FAIL: len(handlers) != 1")
	} else {
		testHandler(&handlers[0], []string{"/foo/*"}, -1, []string{"/usr/bin/python3", "-E", "-u", "/resp.py"})
	}
}
