package main

import (
	"flag"
	"io/ioutil"
	"os"
	"os/user"
	"strconv"
	"syscall"
)

var Daemonize = flag.Bool("daemon", false, "daemonize process")
var PidFile = flag.String("pidfile", "/run/engar.pid", "path to pid file")

func main() {
	var err error
	flag.Parse()
	if *Daemonize {
		pid, _, err := syscall.Syscall(syscall.SYS_FORK, 0, 0, 0)
		if int(pid) == -1 {
			Log("Failed to daemonize: %s", err.Error())
			os.Exit(1)
		}
		if pid != 0 {
			os.Exit(0)
		}
	}

	err = ioutil.WriteFile(*PidFile, []byte(strconv.Itoa(os.Getpid())), os.FileMode(0644))
	if err != nil {
		Log("Failed to write process id: %s", err.Error())
		os.Exit(1)
	}
	if os.Getuid() == 0 {
		user, err := user.Lookup("engar")
		if err != nil {
			Log("Failed to get user engar: %s", err.Error())
			os.Exit(1)
		}
		uid, err := strconv.Atoi(user.Uid)
		if err != nil {
			Log("Failed to parse user ID: %s", err.Error())
			os.Exit(1)
		}
		err = syscall.Setuid(uid)
		if err != nil {
			Log("Failed to drop root privileges: %s", err.Error())
			os.Exit(1)
		}
	}

	Main()
}
