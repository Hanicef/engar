# Engar - A gopher server

Engar is a gopher server, designed for quick deployment and ease of use.

## Installation

Golang must be installed to perform the installation. Install it first if you
haven't already.

To install, simply run: `go get gitlab.com/Hanicef/engar`. This will install
Engar, by default, to `$HOME/go/bin/engar`.

If a different install directory is preferred, you can set the `$GOPATH`
environment variable on your system to the directory you would like to install
Engar to before running `go`.

## Configuration

Start by creating a configuration file, preferrably at
`/etc/engar/default.conf`. This configuration file uses the INI configuration
format.

An example configuration file is this:

```
[server]
; Listen on port 7000
listen = :7000

[content]
; Serve files from /home/engar and use index.txt as index files.
directory = /home/engar
index_file = index.txt
```

### Server

Server configuration is done under the `server` section.

By default, the server is listening on all addresses, port 70. To change this,
set the `listen` field to a different address and port. The expected format is
`HOST:PORT`, where `HOST` can be omitted to listen to all addresses.

### Content serving

Content serving configuration is done under the `content` section.

Files and directories are served from `/srv/gopher` by default. To specify a
different directory to serve content from, set the `directory` field to the
path to serve content from instead.

When the requested file is a directory, Engar looks for an index file in that
directory to serve to the client. By default, that file's name is `INDEX`, but
this can be changed by setting `index_file` to the name to use instead for the
index file.

### Special files

Sometimes, special responses are used for cases where a normal response is
unavailable. There are two cases where this can happen: when a file that
doesn't exist is requested, or when an internal server error occurs. In the
former case, the file located at `/etc/engar/not-found` is served, and in the
latter case, the file at `/etc/engar/error` is served.

Both of these paths can be changed by setting `default_file` and `error_file`,
respectively.

### Handlers

The handler file can be specified by setting `handler_file` to the path to the
handler file; more information on handlers are specified under File serving.

## File serving

By default, files are served from `/srv/gopher`. If the requested path matches
a file, the file's content is served to the client. If it matches a directory,
a file named `INDEX` in that directory is served.

### Index file

Index files are intended to be used as file listings for the current directory.
It is formatted like a standard Gopher directory listing; see RFC 1436 for more
details.

### Handlers

Handlers are programs that is invoked for a specific path, rather than serving
a file. This allows scripts to be executed as responses and enables more
interaction to the client.

Handlers are registered by specifying `index_file` under the `content` section
in the configuration file, and are disabled if this is not present.

A handler is registered with two parts: the path to match for the handler, and
the command to invoke when the path matches the request. Each path is separated
by a colon, and each directory is separated by a forward slash. If a path
doesn't start with a forward slash, only the last element of the path must
match. Otherwise, the entire path must match.

The command specified follows similar rules to the Unix shell. Quotes and
apostrophes can be used to escape spaces and allow spaces as arguments. Pipes
and redirections are not supported, and a single dollar sign is replaced with
the requested path, if present.

When commands are invoked, the full request is piped into the standard input of
the program. The response is read from the standard output, and is forwarded to
the client unmodified.

An example configuration can look like this:

```
# Invoke python for all python sources
*.py /usr/bin/python $

# Respond with the request path for all requests starting with /path
/path/* /usr/bin/awk '{print "You requested: "$1"\r\n.\r\n"}'
```

