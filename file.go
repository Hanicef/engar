package main

import (
	"bufio"
	"bytes"
	"io"
	"net"
	"os"
	"path"
	"path/filepath"
	"unicode"
)

const BinaryCheckCount = 1024

func isFileBinary(reader io.Reader) (bool, error) {
	var b [4]byte
	var r rune
	var c int

	for l := 0; l < BinaryCheckCount; l++ {
		n, err := reader.Read(b[:1])
		if err != nil {
			if err == io.EOF {
				return false, nil
			}
			return false, err
		} else if n < 1 {
			return false, nil
		}

		if b[0] & 0x80 != 0 {
			if b[0] & 0xe0 == 0xc0 {
				n, err = reader.Read(b[1:2])
				if err != nil {
					if err == io.EOF {
						return true, nil
					}
					return false, err
				} else if n < 1 {
					return true, nil
				}
				r = rune(b[0]) & 0x1f
				c = 1
			} else if b[0] & 0xf0 == 0xe0 {
				n, err = reader.Read(b[1:3])
				if err != nil {
					if err == io.EOF {
						return true, nil
					}
					return false, err
				} else if n < 2 {
					return true, nil
				}
				r = rune(b[0]) & 0x0f
				c = 2
			} else if b[0] & 0xf8 == 0xf0 {
				n, err = reader.Read(b[1:4])
				if err != nil {
					if err == io.EOF {
						return true, nil
					}
					return false, err
				} else if n < 3 {
					return true, nil
				}
				r = rune(b[0]) & 0x07
				c = 3
			} else {
				return true, nil
			}
			for i := 0; i < c; i++ {
				if b[i+1] & 0xc0 != 0x80 {
					return true, nil
				}
				r = (r << 6) | rune(b[i+1] & 0x3f)
			}
		} else {
			r = rune(b[0])
		}

		if !unicode.IsGraphic(r) && !unicode.IsSpace(r) {
			return true, nil
		}
	}

	return false, nil
}

func ServeError(conn net.Conn, path string, err error) {
	if os.IsNotExist(err) {
		_, err = conn.Write(bytes.Replace(DefaultResp, Placeholder, []byte(path), 1))
	} else {
		Log("Filesystem error: %s", err.Error())
		_, err = conn.Write(bytes.Replace(ErrorResp, Placeholder, []byte(err.Error()), 1))
	}
	if err != nil {
		Log("Client error: %s", err.Error())
	}
}

func ServeFile(conn net.Conn, reqpath, fullpath string) {
	if Handlers != nil {
		for _, h := range Handlers {
			if h.Match(reqpath) {
				cmd := h.Instantiate(fullpath)
				cmd.Stderr = os.Stderr
				out, err := cmd.StdinPipe()
				if err != nil {
					Log("Handle error: %s", err.Error())
					_, err = conn.Write(bytes.Replace(ErrorResp, Placeholder, []byte(err.Error()), 1))
					if err != nil {
						Log("Client error: %s", err.Error())
					}
					return
				}
				in, err := cmd.StdoutPipe()
				if err != nil {
					Log("Handle error: %s", err.Error())
					_, err = conn.Write(bytes.Replace(ErrorResp, Placeholder, []byte(err.Error()), 1))
					if err != nil {
						Log("Client error: %s", err.Error())
					}
					out.Close()
					return
				}
				defer in.Close()
				err = cmd.Start()
				if err != nil {
					Log("Handle error: %s", err.Error())
					_, err = conn.Write(bytes.Replace(ErrorResp, Placeholder, []byte(err.Error()), 1))
					if err != nil {
						Log("Client error: %s", err.Error())
					}
					out.Close()
					return
				}

				_, err = out.Write([]byte(reqpath))
				if err != nil {
					Log("Handle error: %s", err.Error())
					_, err = conn.Write(bytes.Replace(ErrorResp, Placeholder, []byte(err.Error()), 1))
					if err != nil {
						Log("Client error: %s", err.Error())
					}
					out.Close()
					cmd.Process.Kill()
					return
				}
				err = out.Close()
				if err != nil {
					Log("Handle error: %s", err.Error())
					_, err = conn.Write(bytes.Replace(ErrorResp, Placeholder, []byte(err.Error()), 1))
					if err != nil {
						Log("Client error: %s", err.Error())
					}
					cmd.Process.Kill()
					return
				}
				_, err = io.Copy(conn, in)
				if err != nil {
					Log("Handle error: %s", err.Error())
				}
				err = cmd.Wait()
				if err != nil {
					Log("Handle error: %s", err.Error())
				}
				return
			}
		}
	}

	stat, err := os.Stat(filepath.FromSlash(fullpath))
	if err != nil {
		ServeError(conn, reqpath, err)
		return
	}

	if stat.IsDir() {
		fullpath = path.Join(fullpath, IndexFile)
	}

	file, err := os.Open(filepath.FromSlash(fullpath))
	if err != nil {
		ServeError(conn, reqpath, err)
		return
	}
	defer file.Close()

	isBinary, err := isFileBinary(file)
	if err != nil {
		ServeError(conn, reqpath, err)
		return
	}

	_, err = file.Seek(0, io.SeekStart)
	if err != nil {
		ServeError(conn, reqpath, err)
		return
	}
	if isBinary {
		_, err = io.Copy(conn, file)
		if err != nil {
			Log("Client error: %s", err.Error())
			return
		}
		return
	}

	reader := bufio.NewReader(file)
	for {
		line, err := reader.ReadBytes('\n')
		if err != nil {
			if err == io.EOF {
				break
			}
			Log("Filesystem error: %s", err.Error())
			return
		}
		_, err = conn.Write(append(line[:len(line)-1], '\r', '\n'))
		if err != nil {
			Log("Client error: %s", err.Error())
			return
		}
	}
}
