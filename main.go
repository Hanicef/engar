package main

import (
	"gopkg.in/ini.v1"

	"bufio"
	"bytes"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"net"
	"os"
	"os/exec"
	"os/signal"
	"path"
	"strings"
	"sync"
	"time"
	"unicode"
)

const MaxLineLength = 64

var Placeholder = []byte("$")

type Handler struct {
	Wildcards []string
	Parameter int
	Command []string
}

func (h *Handler) Match(path string) bool {
	if path == "" {
		return false
	}
nextWild:
	for _, w := range h.Wildcards {
		s := path
		if w[0] != '/' {
			if p := strings.LastIndex(s, string(os.PathSeparator)); p != -1 {
				s = s[p+1:]
			}
		} else {
			w = w[1:]
			if s[0] == '/' {
				s = s[1:]
			}
		}
		c := 0
		for i := range s {
			if w[c] == '*' {
				if c+1 == len(w) {
					return true
				} else if w[c+1] == s[i] {
					c += 2
				}
			} else if w[c] != '?' && w[c] != s[i] {
				continue nextWild
			} else {
				c++
			}
		}
		if c == len(w) {
			return true
		}
	}
	return false
}

func (h *Handler) Instantiate(arg string) *exec.Cmd {
	args := h.Command
	if h.Parameter != -1 {
		args = make([]string, len(h.Command))
		copy(args, h.Command)
		args[h.Parameter] = arg
	}
	return exec.Command(h.Command[0], args[1:]...)
}

var ConfigFile = flag.String("config", "/etc/engar/default.conf", "configuration file")

var ListenAddr = ":70"
var Timeout = time.Second * 30

var Directory = "/srv/gopher"
var IndexFile = "INDEX"
var DefaultRespFile = "/etc/engar/not-found"
var ErrorFile = "/etc/engar/error"
var HandlerFile string

var DefaultResp []byte
var ErrorResp []byte
var Handlers []Handler

func Log(format string, a ...interface{}) {
	fmt.Println(time.Now().Format("2006-01-02 15:04:05:"), fmt.Sprintf(format, a...))
}

func LoadConfig() {
	cfg, err := ini.Load(*ConfigFile)
	if err != nil {
		Log("Failed to read configuration file at %s: %s", *ConfigFile, err.Error())
		os.Exit(1)
	}

	server, err := cfg.GetSection("server")
	if err == nil {
		listen, err := server.GetKey("listen")
		if err == nil {
			ListenAddr = listen.String()
		}

		timeout, err := server.GetKey("timeout")
		if err == nil {
			i, err := timeout.Int64()
			if err != nil {
				Log("Configuration parse error: field timeout is not a number")
				os.Exit(1)
			}
			Timeout = time.Second * time.Duration(i)
		}
	}

	content, err := cfg.GetSection("content")
	if err == nil {
		directory, err := content.GetKey("directory")
		if err == nil {
			Directory = directory.String()
		}

		indexFile, err := content.GetKey("index_file")
		if err == nil {
			IndexFile = indexFile.String()
		}

		defaultRespFile, err := content.GetKey("default_file")
		if err == nil {
			DefaultRespFile = defaultRespFile.String()
		}

		errorFile, err := content.GetKey("error_file")
		if err == nil {
			ErrorFile = errorFile.String()
		}

		handlerFile, err := content.GetKey("handler_file")
		if err == nil {
			HandlerFile = handlerFile.String()
		}
	}
}

func Subdivide(n int, s string) []string {
	var a []string
	r := []rune(s)
	for i := 0; i < len(r); i++ {
		for i < len(r) && unicode.IsSpace(r[i]) { i++ }
		j := i
		var b []rune
		for j < len(r) && !unicode.IsSpace(r[j]) {
			if r[j] == '"' {
				for j++; j < len(r) && r[j] != '"'; j++ {
					b = append(b, r[j])
				}
				if j == len(r) {
					Log("Syntax error in %s: line %d: quote not closed", HandlerFile, n)
					os.Exit(1)
				}
			} else if r[j] == '\'' {
				for j++; j < len(r) && r[j] != '\''; j++ {
					b = append(b, r[j])
				}
				if j == len(r) {
					Log("Syntax error in %s: line %d: apostrophe not closed", HandlerFile, n)
					os.Exit(1)
				}
			} else {
				b = append(b, r[j])
			}
			j++
		}
		a = append(a, string(b))
		i = j
	}
	return a
}

func ParseHandlers(file io.Reader) []Handler {
	var handlers []Handler
	reader := bufio.NewReader(file)
	for n := 1; true; n++ {
		line, err := reader.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				break
			}
			Log("Failed to load handlers at %s: %s", HandlerFile, err.Error())
			os.Exit(1)
		}
		line = line[:len(line)-1]
		if len(line) == 0 || line[0] == '#' {
			continue
		}
		div := Subdivide(n, line)
		param := -1
		for i, s := range div[1:] {
			if s == "$" {
				param = i
				break
			}
		}
		handlers = append(handlers, Handler{
			Wildcards: strings.Split(div[0], ":"),
			Parameter: param,
			Command: div[1:],
		})
	}

	return handlers
}

var ConnGroup sync.WaitGroup

func HandleClient(conn net.Conn) {
	var line []byte
	var b [1]byte
	defer conn.Close()
	defer ConnGroup.Done()

	err := conn.SetDeadline(time.Now().Add(Timeout))
	if err != nil {
		Log("Client error: %s", err.Error())
		return
	}

	var i int
	for i = 0; i < MaxLineLength; i++ {
		_, err := conn.Read(b[:])
		if err != nil {
			Log("Client error: %s", err.Error())
			return
		}
		if b[0] == '\r' {
			_, err := conn.Read(b[:])
			if err != nil {
				Log("Client error: %s", err.Error())
				return
			}
			if b[0] == '\n' {
				break
			}
			line = append(line, '\r')
		}
		line = append(line, b[0])
	}
	if i == MaxLineLength {
		_, err = conn.Write(bytes.Replace(ErrorResp, Placeholder, []byte("exceeded maximum line length"), 1))
		if err != nil {
			Log("Client error: %s", err.Error())
		}
		return
	}

	Log("%s -> %s", conn.RemoteAddr().String(), string(line))
	if len(line) > 0 && line[0] == '/' {
		line = line[1:]
	}
	fullpath := string(line)
	if n := strings.IndexByte(fullpath, '\t'); n != -1 {
		fullpath = fullpath[:n]
	}
	fullpath = path.Clean(path.Join(Directory, fullpath))
	if !strings.HasPrefix(fullpath, Directory) {
		_, err = conn.Write(bytes.Replace(DefaultResp, Placeholder, []byte(fullpath), 1))
		if err != nil {
			Log("Client error: %s", err.Error())
		}
		return
	}
	ServeFile(conn, string(line), fullpath)
}

func Main() {
	var err error
	LoadConfig()

	var loadResp = func (file string) []byte {
		resp, err := ioutil.ReadFile(file)
		if err != nil {
			Log("Failed to load response file at %s: %s", file, err.Error())
			os.Exit(1)
		}
		return bytes.ReplaceAll(resp, []byte("\n"), []byte("\r\n"))
	}

	ErrorResp = loadResp(ErrorFile)
	DefaultResp = loadResp(DefaultRespFile)

	if HandlerFile != "" {
		file, err := os.Open(HandlerFile)
		if err != nil {
			Log("Failed to load handlers at %s: %s", HandlerFile, err.Error())
			os.Exit(1)
		}
		Handlers = ParseHandlers(file)
		file.Close()
	}

	listener, err := net.Listen("tcp", ListenAddr)
	if err != nil {
		Log("Failed to start Gopher server on %s: %s", ListenAddr, err.Error())
		os.Exit(1)
	}

	var sigchan = make(chan os.Signal, 1)
	var closed = make(chan int, 1)
	signal.Notify(sigchan, os.Interrupt)
	go func () {
		<-sigchan
		fmt.Fprintln(os.Stderr, "Received SIGINT, exiting...")
		closed <- 0
		listener.Close()
	}()

mainLoop:
	for {
		conn, err := listener.Accept()
		if err != nil {
			select {
				case <-closed:
					ConnGroup.Wait()
					break mainLoop
				default:
					Log("Listener error: %s", err.Error())
					continue mainLoop
			}
		}

		ConnGroup.Add(1)
		go HandleClient(conn)
	}
}
